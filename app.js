const express = require('express')
const cors = require('cors')
const expressLayouts = require('express-ejs-layouts')
const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: true}))


const db = require('./app/models/')
db.mongoose
    .connect(db.url, {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }).then(() => {
        console.log(`Database Connected Successfully.!`)
    }).catch((err) => {
        console.log(`Failed connect to the databse.!`, err)
        process.exit()
    })

const port = 3000

// menggunakan view engine ejs 
app.set('view engine', 'ejs')
app.use(expressLayouts)
app.use(express.static(__dirname + '/public'));


require('./app/routes/product.routes')(app)
require('./app/routes/main.routes')(app)
require('./app/routes/profile.routes')(app)
require('./app/routes/blog.routes')(app)

app.listen(port, () => console.log(`Example app listening on http://localhost:${port} .!`))