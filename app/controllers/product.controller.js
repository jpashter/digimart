const res = require('express/lib/response')
const db = require('../models')

const Product = db.products


exports.findAll = (req, res) => {
     
    Product.find()
    .then((result) => {
        res.send(result)
    }).catch((err) =>{
        res.status(500).send({
            messsage: err.messsage || "Some error while retrieving products.!"
        })
    })

}

exports.main = (req, res) =>{
    Product.find()
    .then((result) => {
        res.render('pages/product/product',{
            layout : 'layouts/mainLayout',
            title   : 'SDG || Products',
            pageTitle : 'Products',
            products : result,
            no:1
        })
    }).catch((err) =>{
        res.status(500).send({
            messsage: err.messsage || "Some error while retrieving products.!"
        })
    })

    // res.render('pages/product/product',{
    //     layout : 'layouts/mainLayout',
    //     title   : 'SDG || Products',
    //     pageTitle : 'Products',
    //     products : Product.find()
    // })
}

