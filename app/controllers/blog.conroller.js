const res = require("express/lib/response")

exports.main =(req,res) => {
    res.render('pages/blog/blog',{
        layout:'layouts/mainLayout',
        title: 'SDB || Blogs',
        pageTitle: 'Blogs'
    })
}

exports.form=(req, res)=>{
    res.render('pages/blog/blogForm',{
        layout:'layouts/mainLayout',
        title: 'SDB || New Blog',
        pageTitle: 'Add New Blog'
    })
}