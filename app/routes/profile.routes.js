module.exports = (app) => {
    const profile = require('../controllers/profile.controller')

    const router = require('express').Router()


    router.get('/', profile.main)

    app.use('/profile', router)

}