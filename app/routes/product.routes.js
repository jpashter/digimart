module.exports = (app) => {
    const products = require('../controllers/product.controller')

    const router = require('express').Router()


    router.get('/', products.main)

    app.use('/product', router)
}