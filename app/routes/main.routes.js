module.exports = (app) => {

    const router = require('express').Router()


    app.get('/', (req, res) =>{
        res.render('dashboard',{
            layout: 'layouts/mainLayout',
            title: 'Dashboard',
            pageTitle: 'Dashboard'
        })
    })



    app.use('', router)
}