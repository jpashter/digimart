module.exports = (app) => {
    const blogs = require('../controllers/blog.conroller')
    const router = require('express').Router()


    router.get('/', blogs.main)
    router.get('/newblog',blogs.form)

    app.use('/blog', router)
}